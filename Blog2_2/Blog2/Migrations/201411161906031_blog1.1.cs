namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blog11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "DateCreated", c => c.DateTime(nullable: false));
            DropColumn("dbo.Posts", "DateCreate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "DateCreate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Posts", "DateCreated");
        }
    }
}
