﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog2.Models;

namespace Blog2.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            //lấy lại các post xem trước đó
            Session["Post"] = post;
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                //tao list cac tag
                List<Tag> Tags = new List<Tag>();
                //tách các tag theo dấu ,
                string[] TagContent = Content.Split(',');
                foreach(string item in TagContent)
                {
                    //tim xem tag content da co hay chua
                    Tag tagExists = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if(ListTag.Count()>0)
                    {
                        //nếu có tag rồi thì add thêm post vào
                        tagExists = ListTag.First();
                        tagExists.Posts.Add(post);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        tagExists = new Tag();
                        tagExists.Content = item;
                        tagExists.Posts = new List<Post>();
                        tagExists.Posts.Add(post);
                    }
                    //add vào list các tag
                    Tags.Add(tagExists);
                }
                //gán list tag cho Post
                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}