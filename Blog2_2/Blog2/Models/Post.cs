﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]//bắt buộc phải nhập dữ liệu
        [StringLength(500,ErrorMessage="So luong ky tu trong khoang 20 - 500",MinimumLength=20)]
        public string Title { set; get; }
        
        [Required]
        [MinLength(50,ErrorMessage="Body toi thieu 50 ky tu")]
        public string Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
             
    }
}