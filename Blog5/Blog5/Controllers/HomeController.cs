﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog5.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "This is my page";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Change Information About Page";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult MyPage()
        {
            ViewBag.view = "Day la noi dung trang cua tui";
            return View();
        }
    }
}
