﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime DateCreated { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileId { set; get; }
    }
}