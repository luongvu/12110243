﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Require]
        [StringLength(100,ErrorMessage="Content co to 10-100 ky tu",MinimumLength=10)]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }

    }
}