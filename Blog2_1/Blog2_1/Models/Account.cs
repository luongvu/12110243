﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Account
    {
       
        public int AccountID { set; get; }
         [DataType(DataType.Password)]
        public String Password { set; get; }
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [StringLength(100,ErrorMessage="so tu vuot qua khoa quy dinh")]
        public String FirstName { set; get; }
         [StringLength(100, ErrorMessage = "so tu vuot qua khoa quy dinh")]
        public String LastName { set; get; }

         public int PostID { set; get; }
         public virtual Post Posts { set; get; }
    }
}