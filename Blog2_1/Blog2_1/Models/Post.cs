﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Post
    {
        public int ID { set; get;}
        [StringLength(500,ErrorMessage="Số lượng từ vượt ra khỏi khoảng quy định",MinimumLength=10)]
        public String Title { set; get; }
        [MinLength(50,ErrorMessage="số tù quá ngắn")]
        public String Body { set; get; }
        public DateTime DateCraeted { set; get; }
        public DateTime DateUpdated { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Account> Accounts { set; get; }

    }
}