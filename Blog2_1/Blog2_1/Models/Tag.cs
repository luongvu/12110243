﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_1.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [StringLength(100,ErrorMessage="so tu ban vua nhap khong nam trong khoang cho phep" ,MinimumLength=10)]        
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}