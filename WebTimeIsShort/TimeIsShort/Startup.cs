﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TimeIsShort.Startup))]
namespace TimeIsShort
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
