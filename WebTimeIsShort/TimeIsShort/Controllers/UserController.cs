﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsShort.Models;
namespace TimeIsShort.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        TimeIsShortEntities db = new TimeIsShortEntities();

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]  
        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(User ur)
        {
            //hợp lệ thì cho
            if(ModelState.IsValid)
            {
                //insert dữ liệu vào bảng User mới vào Model
                db.Users.Add(ur);
                //Lưu vào CSDL
                db.SaveChanges();
            }
          
            return View();
        }
        [HttpGet]
        public ActionResult LogIn()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LogIn(FormCollection f)
        {
            string strTaiKhoan = f["txtTaiKhoan"].ToString();
            string strMatKhau = f.Get("txtMatKhau").ToString();
            User ur = db.Users.SingleOrDefault(n => n.UserName == strTaiKhoan && n.PassWord == strMatKhau);
            if(ur!=null)
            {
                ViewBag.ThongBao = "Access Complete!!!";
                ViewBag.TenUser="Hello "+strTaiKhoan;
                //lưu lại Session
                Session["TaiKhoan"] = ur;
                return View();
               
            }
            ViewBag.ThongBao = "User or PassWord not correct!!!";
            return View();

        }
	}
}