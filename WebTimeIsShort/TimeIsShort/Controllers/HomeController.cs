﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeIsShort.Models;
namespace TimeIsShort.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        //ket noi voi CSDL
        TimeIsShortEntities db = new TimeIsShortEntities();
        public ActionResult Index()
        {
            return View(db.Motivations.ToList());
        }
        public PartialViewResult MotivationNewPartial()
        {
            //lấy dữ liệu từ model lên view
            var listNewMotivation = db.Motivations.Take(3).ToList();
            return PartialView(listNewMotivation);
        }
	}
}